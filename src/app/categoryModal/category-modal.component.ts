import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatDividerModule } from '@angular/material/divider';

@Component({
  selector: 'app-modal',
  templateUrl: './category-modal.component.html',
  styleUrls: ['./category-modal.component.less'],
})
export class CategoryModal implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<CategoryModal>,
    @Inject(MAT_DIALOG_DATA) public data: Object) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    
  }

}

import { NgModule } from '@angular/core';
import {MatButtonModule, MatProgressSpinnerModule, MatCheckboxModule, MatCardModule, MatGridListModule, MatInputModule, MatIconModule, MatDialogModule, MatFormFieldModule} from '@angular/material';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { ModalComponent } from './modal/modal.component';
import { CategoryModal } from './categoryModal/category-modal.component';
import { FormsModule } from '@angular/forms'

@NgModule({
    declarations: [ModalComponent, CategoryModal],
    exports: [
        MatButtonModule,
        MatCheckboxModule,
        MatCardModule,
        MatGridListModule,
        MatInputModule,
        MatIconModule,
        MatDialogModule,
        MatDividerModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
    ],
    imports: [FormsModule],
    entryComponents: [ModalComponent, CategoryModal]
  })
  export class MaterialModule {}
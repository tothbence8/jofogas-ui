import {Component, OnInit, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { ModalComponent } from './modal/modal.component';
import { MatDialog } from '@angular/material';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
    photos;
    categories;
    title = 'jofogas-ui';
    keyWord;
    selectedPhotoDatas;
    sortedCategories;
    loading = {};
    catsTree;

    constructor(private http: HttpClient, public dialog: MatDialog) {
    }

    ngOnInit() {
        
        this.http
            .get('https://jofogas-be.herokuapp.com/api/categories')
            .toPromise()
            .then(data => {
              console.log(data)
              this.categories = this.transformCategories(data, null);
            });
    }

    serchPhoto(){
        this.http
            .get('https://jofogas-be.herokuapp.com/api/photos?text=' + this.keyWord)
            .toPromise()
            .then((data: { photos: any }) => this.photos = data && data.photos && data.photos.photo);
    }

    getPhotoInfo(photoId: string){
      this.loading[photoId] = true;
        this.http
            .get('https://jofogas-be.herokuapp.com/api/photoDatas?text=' + photoId)
            .toPromise()
            .then((data: { photo: object }) => {
              this.loading[photoId] = false;
              this.openDialog(data.photo)/*this.selectedPhotoDatas = data && data.selectedPhotoDatas*/;
            })
    }

    openDialog(data): void {
        console.log(data);
        const dialogRef = this.dialog.open(ModalComponent, {
          data: data,
          maxHeight: '100vh',
        });
    
        /*dialogRef.afterClosed().subscribe(result => {
          this.animal = result;
        });*/
    }
    
    transformCategories(arr, parent) {
      var out = []
      for(var i in arr) {
          if(arr[i].parent_id == parent) {
              var children = this.transformCategories(arr, arr[i].id)
  
              if(children.length) {
                  arr[i].children = children
              }
              out.push(arr[i])
          }
      }
      return out
    }
}


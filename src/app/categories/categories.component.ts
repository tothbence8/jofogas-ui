import { Component, OnInit, Input } from '@angular/core';
import { copyStyles } from '@angular/animations/browser/src/util';
import { MatDialog } from '@angular/material';
import { CategoryModal } from '../categoryModal/category-modal.component'

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.less']
})
export class CategoriesComponent implements OnInit {

  @Input() categories;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    //this.category=this.categories;
    //console.log(!this.categories[3].parent_id);
  }

  

  handleCategoryClick(title, parent_id) {
    this.dialog.open(CategoryModal, {
      data: { title, parent_id }
    });
  }


}
